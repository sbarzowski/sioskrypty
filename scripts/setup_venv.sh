cd ~/ &&
virtualenv venv &&
. venv/bin/activate &&
pip install python-memcached &&
pip install six sqlparse django-appconf &&
pip install psycopg2 # postgres support
pip install librabbitmq uwsgi gevent
