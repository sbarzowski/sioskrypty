#!/bin/bash
set -e
shopt -s nullglob
source ~/venv/bin/activate
for i in /src/*
do
	echo "$i"
	cd "$i"
	pip install -r "$i"/requirements.txt
done
