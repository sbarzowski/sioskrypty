apt-get upgrade -y &&
apt-get install build-essential fpc texlive-latex-base texlive-lang-polish texlive-latex-recommended texlive-fonts-recommended texlive-latex-extra -y && 
apt-get install python-virtualenv libpython-dev -y &&
apt-get install memcached -y &&
apt-get install libpq-dev -y  && # postgres support
apt-get install git -y &&
apt-get install nginx-full -y &&
apt-get install nodejs npm -y &&
apt-get install rabbitmq-server lighttpd -y && #lighttpd must be after nginx or it may accidentaly start on port 80 and some reloading may be necessary
ln -s /usr/bin/nodejs /bin/node || true && #HACK, see http://askubuntu.com/questions/447612/less-css-and-and-node-js-troubles
npm install --global less 
