#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

sudo apt-get install postgresql postgresql-contrib
sudo -u postgres createuser -D -A sio2
sudo -u postgres createdb -O sio2 sio2
sudo -u postgres psql -U postgres -d postgres -c "alter user sio2 with password 'sio2';"
